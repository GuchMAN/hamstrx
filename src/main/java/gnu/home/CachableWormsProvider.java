package gnu.home;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.Cache.ValueWrapper;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.stereotype.Component;
import rx.Observable;
import rx.functions.Func1;

import java.util.List;
import java.util.Objects;

@Component
public class CachableWormsProvider {

    private final WormsProviderImpl wormsProvider;
    private final Cache cache;

    @Autowired
    public CachableWormsProvider(WormsProviderImpl wormsProvider, CacheManager cacheManager) {
        this.wormsProvider = wormsProvider;
        this.cache = cacheManager.getCache("worms");
    }

    public Observable<Object> worms(Familiae familiae) {
        return Observable
                .fromCallable(() -> cache.get(familiae))
                .filter(Objects::nonNull)
                .map(ValueWrapper::get)
                .map(List.class::cast)
                .flatMap((Func1<List, Observable<?>>) Observable::from)
                .switchIfEmpty(getAndCache(familiae));
    }

    private Observable<Worm> getAndCache(Familiae familiae) {
        Observable<Worm> source = invoke(familiae).cache();
        return source.doOnSubscribe(() -> fillCache(familiae, source));
    }

    private void fillCache(Familiae familiae, Observable<Worm> source) {
        source.toList().forEach(worms -> cache.put(familiae, worms));
    }

    private Observable<Worm> invoke(Familiae familiae) {
        return wormsProvider.worms(familiae);
    }

}