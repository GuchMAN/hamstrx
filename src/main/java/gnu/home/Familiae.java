package gnu.home;

import java.util.concurrent.ThreadLocalRandom;

import static java.util.concurrent.ThreadLocalRandom.current;

public enum Familiae {
    Acanthodrilidae,
    Megascolecidae;

    public static Familiae randomFamila() {
        Familiae[] familiaes = Familiae.values();
        return familiaes[current().nextInt(0, familiaes.length)];
    }
}
