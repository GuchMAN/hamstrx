package gnu.home;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import rx.Observable;


@Component
public class WormsProviderImpl {

    @Autowired
    private Ground ground;

    @CachableRx
    public Observable<Worm> worms(Familiae familiae) {
        return Observable.fromCallable(() -> ground.worms(10))
                .flatMap(Observable::from)
                .filter(worm -> worm.getFamiliae() == familiae);
    }
}
