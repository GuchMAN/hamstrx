package gnu.home;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class HamsteRxApplication {

	public static void main(String[] args) {
		SpringApplication.run(HamsteRxApplication.class, args);
	}
}
