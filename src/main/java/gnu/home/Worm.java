package gnu.home;

import lombok.*;

import java.util.UUID;

import static java.util.UUID.randomUUID;

@Getter
@EqualsAndHashCode
@ToString
@AllArgsConstructor
public class Worm {
    private final String id = UUID.randomUUID().toString();
    private final String name;
    private final Familiae familiae;

    public Worm() {
        this.name = randomUUID().toString();
        this.familiae = Familiae.randomFamila();
    }
}
