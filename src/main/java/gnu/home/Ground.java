package gnu.home;

import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;
import static java.util.stream.IntStream.range;


@Service
public class Ground {
    public List<Worm> worms(int count){
        return range(0, count)
                .mapToObj(i -> new Worm())
                .collect(toList());
    }
}
