package gnu.home;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.stream.IntStream;

import static gnu.home.Familiae.Acanthodrilidae;
import static java.util.stream.Collectors.toList;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.*;
import static org.springframework.test.annotation.DirtiesContext.MethodMode.AFTER_METHOD;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(methodMode = AFTER_METHOD)
public class HamsteRxApplicationTests {

    @Autowired
    private CachableWormsProvider wormsProvider;

    @MockBean
    private Ground ground;

    private Familiae familiaeToGet = Acanthodrilidae;

    @Test
    @Ignore
    public void shouldTakeWormsFromCacheInSecondTime() {
        wormsInTheGround(5);

        getWorms();
        getWorms();

        verify(ground, only()).worms(anyInt());
    }

    @Test
    public void wormsRetriedFromCacheTheSameAsOriginal() {
        wormsInTheGround(5);

        List<Worm> worms = getWorms();
        List<Worm> cachedWorms = getWorms();

        verify(ground, only()).worms(anyInt());
        assertEquals(worms, cachedWorms);
    }

    private List<Worm> getWorms() {
        return (List) wormsProvider.worms(familiaeToGet)
                .toList()
                .toBlocking()
                .first();
    }

    private void wormsInTheGround(int count) {
        List<Worm> worms = IntStream.range(0, count)
                .mapToObj(i -> new Worm(String.valueOf(i), familiaeToGet))
                .collect(toList());
        when(ground.worms(anyInt()))
                .thenReturn(worms);
    }
}
